'use client'
import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { jwtDecode, JwtPayload } from 'jwt-decode';
import Cookies from "js-cookie";
import { useAuth } from '@/context/AuthContext';
import { useRouter } from 'next/navigation'
import { useThemeContext } from '@/context/ThemeContext';
import { useTheme } from '@mui/system';
import getLPTheme from '@/app/getLPTheme';
import { PaletteMode } from '@mui/material';

function Copyright(props: any) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://stag.1click.tech/">
        1 Click Tech
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const defaultTheme = createTheme();

export default function SignIn() {
  const router = useRouter()
  const [isSubmitting, setIsSubmitting] = React.useState(false);
  const { login } = useAuth();
  const mode: PaletteMode = (Cookies.get('themeMode') as PaletteMode) || 'dark';
  const LPtheme = createTheme(getLPTheme('dark'));

  const handleSignIn = async (data : FormData) => {
    try {
      const baseUrl = process.env.NEXT_PUBLIC_AUTH_BASE_URL;

      if (!baseUrl) {
        console.error("Base URL is not defined");
        return;
      }

      const response = await fetch(`${baseUrl}/auth/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username: data.get('email'),
          password: data.get('password'),
        }),
      });

      if (response.ok) {
        const data = await response.json();
        const token = data.token;
        const decodedToken = jwtDecode(token);
        const expirationTime = decodedToken && decodedToken.exp ? decodedToken.exp * 1000 : null;

        Cookies.set("accessToken", token, {
          secure: true,
          expires: expirationTime !== null ? new Date(expirationTime) : undefined,
        });
        Cookies.set("refreshToken", data.refreshToken, {
          secure: true,
          expires: expirationTime !== null ? new Date(expirationTime) : undefined,
        });
        Cookies.set("username", data.userData.username, {
          secure: true,
          expires: expirationTime !== null ? new Date(expirationTime) : undefined,
        });
        login(token, data.userData.username);
        router.push('/');
      } else {
        //
      }
    } catch (error) {
      //
    } finally {
      setIsSubmitting(false);
    }
  };
const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
  setIsSubmitting(true);
  event.preventDefault();
    const data = new FormData(event.currentTarget);

    try {
      await handleSignIn(data);
    } catch (error) {
      // 
    }
};

return (
  <ThemeProvider theme={LPtheme}>
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            required
            fullWidth
            size="small"
            variant="outlined"
            id="email"
            arial-label="Email Address"
            name="email"
            placeholder="Email"
            autoFocus
          />
          <TextField
            margin="normal"
            required
            fullWidth
            size="small"
            variant="outlined"
            name="password"
            arial-label="Password"
            type="password"
            id="password"
            placeholder="Password"
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            disabled={isSubmitting}
          >
            {isSubmitting ? 'Signing In...' : 'Sign In'}
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
      <Copyright sx={{ mt: 8, mb: 4 }} />
    </Container>
  </ThemeProvider>
);
}