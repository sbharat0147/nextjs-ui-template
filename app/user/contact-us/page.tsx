'use client'
import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import MenuItem from '@mui/material/MenuItem';
import { jwtDecode } from 'jwt-decode';
import Cookies from 'js-cookie';
import { useRouter } from 'next/navigation';
import { useAuth } from '@/context/AuthContext';
import getLPTheme from "../../getLPTheme";
import { useTheme } from '@mui/system';
import { alpha } from '@mui/material';
import { useThemeContext } from '@/context/ThemeContext';

export default function ContactUs() {
    const { mode } = useThemeContext();
    const LPtheme = createTheme(getLPTheme(mode));
    const theme = useTheme();
    return (
        <ThemeProvider theme={LPtheme}>
        <Container
            id="testimonials"
            sx={{
                pt: { xs: 4, sm: 12 },
                pb: { xs: 8, sm: 16 },
                position: 'relative',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                gap: { xs: 3, sm: 6 },
            }}
        >
            <Box
                sx={{
                    width: { sm: '100%', md: '60%' },
                    textAlign: { sm: 'left', md: 'center' },
                    p: 2,
                    backgroundSize: 'cover',
                    borderRadius: '10px',
                    outline: '1px solid',
                    outlineColor:
                    theme.palette.mode === 'light'
                        ? alpha('#BFCCD9', 0.5)
                        : alpha('#9CCCFC', 0.1),
                    boxShadow:
                    theme.palette.mode === 'light'
                        ? `0 0 12px 8px ${alpha('#9CCCFC', 0.2)}`
                        : `0 0 24px 12px ${alpha('#033363', 0.2)}`,
                }}
            >
            <CssBaseline/>
            <Box
            sx={{
                
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}
            >
                <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5" sx={{mb: 5}}>
                    Contact Us
                </Typography>
                <Box component="form" noValidate sx={{ mt: 3 }}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                name="name"
                                hiddenLabel
                                size="small"
                                variant="outlined"
                                required
                                fullWidth
                                id="name"
                                aria-label="First Name"
                                placeholder="First Name"
                                autoFocus
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                hiddenLabel
                                size="small"
                                variant="outlined"
                                required
                                fullWidth
                                id="surname"
                                aria-label="Last Name"
                                name="surname"
                                placeholder="Last Name"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="email"
                                hiddenLabel
                                size="small"
                                variant="outlined"
                                required
                                fullWidth
                                id="email"
                                aria-label="Email Address"
                                placeholder="Email Address"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="subject"
                                hiddenLabel
                                size="small"
                                variant="outlined"
                                required
                                fullWidth
                                id="subject"
                                aria-label="Subject"
                                placeholder="Subject"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                id="message"
                                name="message"
                                hiddenLabel
                                size="small"
                                variant="outlined"
                                fullWidth
                                multiline
                                rows={7}
                                aria-label="Enter your message."
                                placeholder="Your message"
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                        // disabled={isSubmitting}
                        >
                        {/* {isSubmitting ? 'Getting you registered...' : 'Sign Up'} */}
                        Submit
                    </Button>
                   
                
                </Box>
            </Box>
            </Box>
        </Container>
        </ThemeProvider>
    );
}