import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import IconButton from '@mui/material/IconButton';
import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import TwitterIcon from '@mui/icons-material/X';
import InstagramIcon from "@mui/icons-material/Instagram";
import FacebookIcon from "@mui/icons-material/Facebook";
import LocationOnIcon from '@mui/icons-material/LocationOn';
import CallIcon from '@mui/icons-material/Call';
import EmailIcon from '@mui/icons-material/Email';
import YouTubeIcon from '@mui/icons-material/YouTube';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';

const logoStyle = {
    width: '140px',
    height: 'auto',
};

function AddressList() {
    return (
        <Box>
            <Typography variant="body2" color="text.secondary" mt={1}>
                <IconButton><LocationOnIcon/></IconButton>
                1 A-26, DLF Shopping Mall, Gurugram, Harayana, India
            </Typography>
            <Typography variant="body2" color="text.secondary" mt={1}>
                <IconButton><CallIcon/></IconButton>
                +91 9889881006
            </Typography>
            <Typography variant="body2" color="text.secondary" mt={1}>
                <IconButton><EmailIcon/></IconButton>
                it@1click.tech
            </Typography>
        </Box>
    );
}

function Handels(){
    return (
        <Stack
            direction="row"
            justifyContent="left"
            spacing={1}
            useFlexGap
            sx={{
                color: 'text.secondary',
            }}
        >
            <IconButton
                color="inherit"
                href="https://www.linkedin.com/company/1clicktechglobalservices/about/"
                aria-label="Facebook"
                sx={{ alignSelf: 'center' }}
            >
                <FacebookIcon/>
            </IconButton>
            <IconButton
                color="inherit"
                href="https://www.linkedin.com/company/1clicktechglobalservices/about/"
                aria-label="Instagram"
                sx={{ alignSelf: 'center' }}
            >
                <InstagramIcon />
            </IconButton>
            <IconButton
                color="inherit"
                href="https://www.linkedin.com/company/1clicktechglobalservices/about/"
                aria-label="Twitter"
                sx={{ alignSelf: 'center' }}
            >
                <TwitterIcon />
            </IconButton>
            <IconButton
                color="inherit"
                href="https://www.linkedin.com/company/1clicktechglobalservices/about/"
                aria-label="LinkedIn"
                sx={{ alignSelf: 'center' }}
            >
                <LinkedInIcon />
            </IconButton>
            <IconButton
                color="inherit"
                href="https://www.linkedin.com/company/1clicktechglobalservices/about/"
                aria-label="YouTube"
                sx={{ alignSelf: 'center' }}
            >
                <YouTubeIcon />
            </IconButton>
            <IconButton
                color="inherit"
                href="https://www.linkedin.com/company/1clicktechglobalservices/about/"
                aria-label="WhatApp"
                sx={{ alignSelf: 'center' }}
            >
                <WhatsAppIcon />
            </IconButton>
        </Stack>
    );
}

function Copyright() {
    return (
        <Box>
            <Typography variant="body2" color="text.secondary" mt={1}>
                {'Copyright © '}
                <Link color="inherit" href="https://stag.1click.tech/">
                    1 Click Tech
                </Link>{' '}
                {new Date().getFullYear()}
            </Typography>
        </Box>
    );
}

function NewsLetter() {
    return (
        <Box>
            <Typography variant="body2" fontWeight={600} gutterBottom>
                Subscribe to Newsletter
            </Typography>
            <Stack direction="row" spacing={1} useFlexGap>
                <TextField
                    id="outlined-basic"
                    hiddenLabel
                    size="small"
                    variant="outlined"
                    fullWidth
                    aria-label="Enter your email address"
                    placeholder="Your email address"
                    inputProps={{
                        //   autocomplete: 'off',
                        //   aria-label: 'Enter your email address',
                    }}
                />
                <Button variant="contained" color="primary" sx={{ flexShrink: 0 }}>
                    Subscribe
                </Button>
            </Stack>
        </Box>
    );
}
export default function Footer() {
    return (
        <Container
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                gap: { xs: 4, sm: 8 },
                py: { xs: 8, sm: 10 },
                textAlign: { sm: 'center', md: 'left' },
            }}
        >
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: { xs: 'column', sm: 'row' },
                    width: '100%',
                    justifyContent: 'space-between',
                }}
            >
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        gap: 4,
                        minWidth: { xs: '100%', sm: '60%' },
                    }}
                >
                    <Box sx={{ width: { xs: '100%', sm: '60%' } }}>
                        <Handels/>
                        <AddressList/>
                    </Box>
                </Box>
                <Box
                    sx={{
                        display: { xs: 'none', sm: 'flex' },
                        flexDirection: 'column',
                        gap: 1,
                    }}
                >
                    <Typography variant="body2" fontWeight={600}>
                        Product
                    </Typography>
                    <Link color="text.secondary" href="#">
                        Features
                    </Link>
                    <Link color="text.secondary" href="#">
                        Testimonials
                    </Link>
                    <Link color="text.secondary" href="#">
                        Highlights
                    </Link>
                    <Link color="text.secondary" href="#">
                        Pricing
                    </Link>
                    <Link color="text.secondary" href="#">
                        FAQs
                    </Link>
                </Box>
                <Box
                    sx={{
                        display: { xs: 'none', sm: 'flex' },
                        flexDirection: 'column',
                        gap: 1,
                    }}
                >
                    <Typography variant="body2" fontWeight={600}>
                        Company
                    </Typography>
                    <Link color="text.secondary" href="/about-us">
                        About us
                    </Link>
                    <Link color="text.secondary" href="/user/contact-us">
                        Contact
                    </Link>
                    <Link color="text.secondary" href="#">
                        Careers
                    </Link>
                    <Link color="text.secondary" href="#">
                        Press
                    </Link>
                </Box>
            </Box>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    pt: { xs: 4, sm: 8 },
                    width: '100%',
                    borderTop: '1px solid',
                    borderColor: 'divider',
                }}
            >
                <div>
                    <Link color="text.secondary" href="#">
                        Privacy Policy
                    </Link>
                    <Typography display="inline" sx={{ mx: 0.5, opacity: 0.5 }}>
                        &nbsp;•&nbsp;
                    </Typography>
                    <Link color="text.secondary" href="#">
                        Terms of Service
                    </Link>
                    <Copyright />
                </div>
                <NewsLetter/>
            </Box>
        </Container>
    );
}