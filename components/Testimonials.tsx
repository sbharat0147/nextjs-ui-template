import * as React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';

const userTestimonials = [
  {
    avatar: <Avatar alt="Soniya" src="/static/images/avatar/soniya.jpg" />,
    name: 'Soniya',
    occupation: 'CEO',
    testimonial:
        "In my role as the CEO, I am thrilled to share my thoughts on our platform. Its simplicity and user-friendliness have significantly simplified my life. I deeply appreciate the creators for delivering a solution that not only meets but exceeds user expectations. Our platform stands as a testament to our commitment to providing an efficient and effective solution.",
  },
  {
    avatar: <Avatar alt="Sapna" src="/static/images/avatar/sapna.jpg" />,
    name: 'Sapna',
    occupation: 'Sr Architect',
    testimonial:
        "The comprehensive testing and exam platform we've created is truly remarkable. Its seamless functionality and user-friendly design make it a standout solution in the education technology landscape. I'm proud to be part of a team that has crafted such an innovative and impactful tool for educators and students.",
  },
  {
    avatar: <Avatar alt="Mayuri" src="/static/images/avatar/mayuri.jpg" />,
    name: 'Mayuri',
    occupation: 'Lead Product Designer',
    testimonial:
        "As the Lead Product Designer, I can't help but commend the exceptional customer support offered by our platform. The team behind this product, which I am a part of, has always been quick to respond and incredibly helpful. It's reassuring to witness our unwavering commitment to our users.",
  },
  {
    avatar: <Avatar alt="Lavina" src="/static/images/avatar/lavina.jpg" />,
    name: 'Lavina',
    occupation: 'AI Engineer',
    testimonial:
        "Working on this platform has been an incredible experience. The AI-driven features bring a new level of intelligence to assessments, providing a personalized and adaptive learning experience for users. It's rewarding to see our efforts contribute to the advancement of education technology.",
  },
  {
    avatar: <Avatar alt="Sneha" src="/static/images/avatar/1.jpg" />,
    name: 'Sneha',
    occupation: 'DevOps Engineer',
    testimonial:
        "Being part of the team that developed this comprehensive test and exam platform has been fulfilling. The seamless integration of technologies ensures educators and students have a powerful tool at their disposal. It's rewarding to see how our platform simplifies and enhances the assessment process.",
  },

  {
    avatar: <Avatar alt="Nitin Rana" src="/static/images/avatar/nitinrana.jpg" />,
    name: 'Nitin Rana',
    occupation: 'Full Stack Developer',
    testimonial:
        "As a Full-stack Developer, I am thrilled to be part of our exceptional team. The collaborative environment and cutting-edge technologies have made our development process truly exciting. It's a pleasure contributing to a platform that pushes boundaries and fosters innovation.",

  },
  {
    avatar: <Avatar alt="Rijin Raju" src="/static/images/avatar/rijinraju.jpg" />,
    name: 'Rijin Raju',
    occupation: 'Backend Developer',
    testimonial:
        "The robust foundation we've built for our testing and exam platform sets it apart. As a backend developer, it's satisfying to know that our focus on efficient data processing and secure infrastructure contributes to a high-performance solution. This platform is a testament to our commitment to excellence in education technology.",
  }
];

export default function Testimonials() {
  return (
      <Container
          id="testimonials"
          sx={{
            pt: { xs: 4, sm: 12 },
            pb: { xs: 8, sm: 16 },
            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            gap: { xs: 3, sm: 6 },
          }}
      >
        <Box
            sx={{
              width: { sm: '100%', md: '60%' },
              textAlign: { sm: 'left', md: 'center' },
            }}
        >
          <Typography component="h2" variant="h4" color="text.primary">
            Testimonials
          </Typography>
          <Typography variant="body1" color="text.secondary">
            See what our team members or customers has to say. Discover how we excel in
            efficiency, durability, and satisfaction. Join us for quality, innovation,
            and reliable support.
          </Typography>
        </Box>
        <Grid container spacing={2} sx={{ overflowX: 'auto', flexWrap: 'nowrap' }}>
          {userTestimonials.map((testimonial, index) => (
              <Grid item xs={12} sm={6} md={4} key={index} sx={{ display: 'flex' }}>
                <Card
                    sx={{
                      display: 'flex',
                      flexDirection: 'column',
                      justifyContent: 'space-between',
                      flexGrow: 1,
                      p: 1,
                      minWidth: '300px',
                    }}
                >
                  <CardContent>
                    <Typography variant="body2" color="text.secondary">
                      {testimonial.testimonial}
                    </Typography>
                  </CardContent>
                  <Box
                      sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        pr: 2,
                      }}
                  >
                    <CardHeader
                        avatar={testimonial.avatar}
                        title={testimonial.name}
                        subheader={testimonial.occupation}
                    />
                  </Box>
                </Card>
              </Grid>
          ))}
        </Grid>
      </Container>
  );
}